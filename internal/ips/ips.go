// Package ips controls the Inductive Power System (charging system) on the Deep
// Profiler DSC.
package ips

import (
	"context"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
	"google.golang.org/grpc"
)

// Status registers
const (
	OutReg uint32 = 0x1006
	InReg         = 0x100e
)

// Status bits
const (
	BiasEnabled        uint32 = 0x01
	ChargingEnabled           = 0x02
	ChargingInactive          = 0x08
	ChargingLowCurrent        = 0x10
)

// Digital outputs for IPS control
const (
	Enable       string = "130"
	BiasEnable          = "8100_LCD_D0"
	ChargeEnable        = "8100_LCD_D1"
)

type Ips struct {
	client pb.FpgaMemClient
}

func New(conn *grpc.ClientConn) *Ips {
	return &Ips{
		client: pb.NewFpgaMemClient(conn),
	}
}

func peek(client pb.FpgaMemClient, msg *pb.AddrMsg) (*pb.ValueMsg, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	return client.Peek(ctx, msg)
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func (ips *Ips) Status() (uint32, error) {
	msg := &pb.AddrMsg{Address: OutReg, Size: pb.Size_WORD}
	resp, err := peek(ips.client, msg)
	if err != nil {
		return 0, err
	}
	val := resp.Value
	msg.Address = InReg
	resp, err = peek(ips.client, msg)
	if err != nil {
		return 0, err
	}

	return (val | resp.Value) & 0xff, nil
}

func (ips *Ips) Enable() error {
	err := dioset(ips.client, Enable, pb.DioState_HIGH)
	if err != nil {
		return err
	}

	err = dioset(ips.client, BiasEnable, pb.DioState_HIGH)
	if err != nil {
		dioset(ips.client, Enable, pb.DioState_LOW)
		return err
	}

	err = dioset(ips.client, ChargeEnable, pb.DioState_HIGH)
	if err != nil {
		dioset(ips.client, BiasEnable, pb.DioState_LOW)
		dioset(ips.client, Enable, pb.DioState_LOW)
	}

	return err
}

func (ips *Ips) Disable() error {
	for _, name := range []string{Enable, BiasEnable, ChargeEnable} {
		err := dioset(ips.client, name, pb.DioState_LOW)
		if err != nil {
			return err
		}
	}

	return nil
}
