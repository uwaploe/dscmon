// Package sampler provides an interface to a set of ADC channels
package sampler

type Adc interface {
	ReadCounts(n uint) (int, error)
	VoltsPerCount(n uint) float32
}

type Sampler struct {
	dev   Adc
	chans []uint
}

func New(dev Adc, chans []uint) *Sampler {
	s := &Sampler{dev: dev}
	s.chans = make([]uint, len(chans))
	copy(s.chans, chans)
	return s
}

func (s *Sampler) Sample() []float32 {
	rec := make([]float32, len(s.chans))
	for i, j := range s.chans {
		x, _ := s.dev.ReadCounts(j)
		rec[i] = float32(x) * s.dev.VoltsPerCount(j)
	}

	return rec
}
