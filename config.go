package main

import yaml "gopkg.in/yaml.v2"

var defConfig = `
channels:
  - name: T_heatsink
    cnum: 3
    units: degC
    c: [-6.02, 36.71]
    scale: 1000.0
  - name: T_ambient
    cnum: 4
    units: degC
    c: [-6.02, 36.71]
    scale: 1000.0
  - name: humidity
    cnum: 5
    units: "%"
    c: [-23.82, 48.38]
    scale: 10.0
  - name: v12
    cnum: 6
    units: amps
    c: [0., 1.]
    scale: 1000.0
`

type Channel struct {
	Name  string    `yaml:"name"`
	Cnum  uint      `yaml:"cnum"`
	Units string    `yaml:"units"`
	C     []float32 `yaml:"c,flow"`
	Scale float32   `yaml:"scale"`
}

type progCfg struct {
	Channels []Channel `yaml:"channels"`
}

func (cfg *progCfg) Indicies() []uint {
	idx := make([]uint, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		idx = append(idx, c.Cnum-1)
	}

	return idx
}

func (cfg *progCfg) Names() []string {
	names := make([]string, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		names = append(names, c.Name)
	}

	return names
}

func (cfg *progCfg) Funcs() []ScaleFunc {
	f := make([]ScaleFunc, 0, len(cfg.Channels))
	for _, c := range cfg.Channels {
		f = append(f, polyMult(c.C, c.Scale))
	}

	return f
}

func loadConfig(contents []byte) (progCfg, error) {
	cfg := progCfg{}
	var err error

	if contents == nil {
		err = yaml.Unmarshal([]byte(defConfig), &cfg)
	} else {
		err = yaml.Unmarshal(contents, &cfg)
	}

	return cfg, err
}
