// Monitor the ADC channels and IPS status on the Deep Profiler Docking Station
// Controller and publish the data to a Redis pub-sub channel.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/dscmon/internal/adc"
	"bitbucket.org/uwaploe/dscmon/internal/ips"
	"bitbucket.org/uwaploe/dscmon/internal/sampler"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc"
)

const Usage = `Usage: dscmon [options] [cfgfile]

Monitor the ADC channels and IPS status on the DSC and publish the data
on a Redis pub-sub channel. The optional cfgfile contains coefficients
for the ADC inputs.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg = flag.Bool("dumpcfg", false,
		"Dump default configuration to stdout and exit")
	rpcAddr      string        = "localhost:10101"
	rdAddr       string        = "localhost:6379"
	rdChan       string        = "data.eng"
	tickInterval time.Duration = time.Second * 30
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&tickInterval, "interval", tickInterval,
		"Sampling interval")
	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for the TS-FPGA gRPC server")
	flag.StringVar(&rdAddr, "rd-addr", rdAddr,
		"host:port for the Redis server")
	flag.StringVar(&rdChan, "chan", rdChan, "Redis pub-sub channel")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpCfg {
		fmt.Fprintf(os.Stdout, "---%s\n", defConfig)
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

type ScaleFunc func(float32) float32

// Return a function that performs the polynomial multiplication
//   y = C[0] + C[1]*x + C[2]*x^2 + ...
func polyMult(c []float32, scale float32) ScaleFunc {
	return ScaleFunc(func(x float32) float32 {
		y := float32(0)
		for i := len(c) - 1; i > 0; i-- {
			y = x * (c[i] + y)
		}
		if scale != 0 {
			return float32(math.Floor(float64((y + c[0]) * scale)))
		} else {
			return y + c[0]
		}
	})
}

type dataRecord struct {
	Src  string           `json:"name"`
	T    [2]int64         `json:"t"`
	Data map[string]int64 `json:"data"`
}

func timestamp(t time.Time) [2]int64 {
	tt := t.Truncate(time.Microsecond)
	return [2]int64{tt.Unix(), int64(tt.Nanosecond()) / 1000}
}

func pubRecord(conn redis.Conn, cname string, rec dataRecord) error {
	b, err := json.Marshal(rec)
	if err != nil {
		return err
	}
	_, err = conn.Do("PUBLISH", cname, b)
	return err
}

func main() {
	args := parseCmdLine()

	var contents []byte
	if len(args) >= 1 {
		var err error
		contents, err = ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}
	}

	cfg, err := loadConfig(contents)
	if err != nil {
		log.Fatalf("Cannot parse config file: %v", err)
	}

	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot access Redis server: %v", err)
	}

	gconn, err := grpcConnect(rpcAddr)
	if err != nil {
		log.Fatalf("Cannot access TS-FPGA server: %v", err)
	}
	defer gconn.Close()

	dev, err := adc.New(gconn)
	if err != nil {
		log.Fatal(err)
	}

	err = dev.Configure(adc.Size16Bits, adc.GainX1, adc.AnSelPin77)
	if err != nil {
		log.Fatal(err)
	}

	charger := ips.New(gconn)

	s := sampler.New(dev, cfg.Indicies())
	funcs := cfg.Funcs()
	names := cfg.Names()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	dr := dataRecord{
		Src:  "dock",
		Data: make(map[string]int64),
	}
	ticker := time.NewTicker(tickInterval)
	for {
		select {
		case t0 := <-ticker.C:
			rec := s.Sample()
			dr.T = timestamp(t0)
			for i, x := range rec {
				dr.Data[names[i]] = int64(funcs[i](x))
			}
			status, err := charger.Status()
			if err != nil {
				log.Printf("Cannot read IPS status: %v", err)
			}
			dr.Data["ips_status"] = int64(status)
			if err := pubRecord(conn, rdChan, dr); err != nil {
				log.Printf("Cannot publish message: %v", err)
			}
		case <-sigs:
			return
		}
	}
}
